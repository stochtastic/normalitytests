import unittest
import numpy as np
import normalitytests as nt

class TestNormalityTests(unittest.TestCase):

    def test_ks(self):
        np.random.seed(seed=1)
        # The null:
        yn = np.random.normal(0.0,1.0,1000) # Should not reject most of the time
        H, pval, ks_stat = nt.ks(yn,alpha=0.05)
        self.assertFalse(H)
        self.assertEqual(pval, 0.17088563112849142)
        self.assertEqual(ks_stat, 0.034903542365887219)
        # OR: Uniform (non-normal)
        np.random.seed(seed=1)
        yu = np.random.uniform(0.0,1.0,1000) # Should reject most of the time
        H, pval, ks_stat = nt.ks(yu,alpha=0.05)
        self.assertTrue(H)
        self.assertEqual(pval, 0.0)
        self.assertEqual(ks_stat, 0.50004562895035254)

    def test_lf(self):
        np.random.seed(seed=1)
        # The null:
        yn = np.random.normal(0.0,1.0,1000) # Should not reject most of the time
        H, pval, lf_stat, lf_crit, lf_null = nt.lf(yn,nboot=1000,alpha=0.05)
        self.assertFalse(H)
        self.assertEqual(pval, 0.41000000000000003)
        self.assertEqual(lf_stat, 0.034903542365887219)
        self.assertEqual(lf_crit, 0.05443035010596814)
        # OR: Uniform (non-normal)
        np.random.seed(seed=1)
        yu = np.random.uniform(0.0,1.0,1000) # Should reject most of the time
        H, pval, lf_stat, lf_crit, lf_null = nt.lf(yu,nboot=1000,alpha=0.05)
        self.assertTrue(H)
        self.assertEqual(pval, 0.0)
        self.assertEqual(lf_stat, 0.50004562895035254)
        self.assertEqual(lf_crit, 0.47587435170827025)

    def test_ksprb(self):
        np.random.seed(seed=1)
        # The null:
        yn = np.random.normal(0.0,1.0,1000) # Should not reject most of the time
        H, pval, ksprb_stat, ksprb_crit, ksprb_null = \
            nt.ksprb(yn,nboot=1000,alpha=0.05)
        self.assertFalse(H)
        self.assertEqual(pval, 0.129)
        self.assertEqual(ksprb_stat, 0.034903542365887219)
        self.assertEqual(ksprb_crit, 0.038632857331825525)
        # OR: Uniform (non-normal)
        np.random.seed(seed=1)
        yu = np.random.uniform(0.0,1.0,1000) # Should reject most of the time
        H, pval, ksprb_stat, ksprb_crit, ksprb_null = \
            nt.ksprb(yu,nboot=1000,alpha=0.05)
        self.assertTrue(H)
        self.assertEqual(pval, 0.0)
        self.assertEqual(ksprb_stat, 0.50004562895035254)
        self.assertEqual(ksprb_crit, 0.46448007407865993)

if __name__ == '__main__':
    unittest.main()
