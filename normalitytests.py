import numpy as np
from scipy.stats import kstest
from statsmodels.distributions.empirical_distribution import ECDF
from scipy.interpolate import interp1d

def ks(y, alpha=None):
    """Performs KS test of normality.

    Note that the null is for a *unit normal*, so one will generally wish to
    normalize the input prior to testing (unless the desired test is
    specifically against a unit normal).

    Example:
import numpy as np
import normalitytests as nt
# The null:
yn = np.random.normal(0.0,1.0,100) # Should not reject most of the time
H, pval, ks_stat = nt.ks(yn,alpha=0.05)
# OR: Uniform (non-normal)
yu = np.random.uniform(0.0,1.0,100) # Should reject most of the time
H, pval, ks_stat = nt.ks(yu,alpha=0.05)
# OR: Normal, but not unit normal
ynn = np.random.uniform(-2.0,5.0,100) # Should reject most of the time
H, pval, ks_stat = nt.ks(ynn,alpha=0.05)
# OR: Normal, but not unit normal --- standardized prior to test
from sklearn.preprocessing import scale
H, pval, ks_stat = nt.ks(scale(ynn),alpha=0.05)

    Args:
        y: a real vector, assumed to be uniformly sampled.
        alpha: significance level (0 < alpha < 1, and default is 0.05).

    Returns:
        reject: logical, True if the null is rejected at the chosen
            significance level.
        pval: probability, under the null hypothesis, of obtaining a test
            statistic at least as extreme as that which was observed.
        ksprb_stat: the sample KS statistic.
    """

    # Input parsing:
    if not alpha: alpha = 0.05

    # Get sample KS statistic
    ksprb_stat, pval = kstest(y, 'norm')

    # Assess result:
    if pval < alpha:
        reject = True
    else:
        reject = False

    # Return
    return reject, pval, ksprb_stat

def lf(y, nboot=None, alpha=None):
    """Performs Lilliefors test of normality.

    Since the standard KS null is for a *unit normal*, the Lilliefors test was
    developed to allow for consistent, unbiased testing against the null of
    the general class of normal distributions. It uses estimates of the
    population mean and variance from the data to estimate a null distribution
    of the KS statistic, testing against that.

    Examples:
import numpy as np
import normalitytests as nt
# The null:
yn = np.random.normal(0.0,1.0,100) # Should not reject most of the time
H, pval, lf_stat, lf_crit, lf_null = nt.lf(yn,nboot=1000,alpha=0.05)
# OR: Uniform (non-normal)
yu = np.random.uniform(0.0,1.0,100) # Should reject most of the time
H, pval, lf_stat, lf_crit, lf_null = nt.lf(yu,nboot=1000,alpha=0.05)
# OR: Normal, but not unit normal
ynn = np.random.uniform(-2.0,5.0,100) # Should reject most of the time
H, pval, lf_stat, lf_crit, lf_null = nt.lf(ynn,nboot=1000,alpha=0.05)
# OR: Normal, but not unit normal --- standardized prior to test
from sklearn.preprocessing import scale
H, pval, lf_stat, lf_crit, lf_null = nt.lf(scale(ynn),nboot=1000,alpha=0.05)

    Args:
        y: a real vector, assumed to be uniformly sampled.
        nboot: Integer number of bootstrap samples to use (default is 1000).
        alpha: significance level (0 < alpha < 1, and default is 0.05).

    Returns:
        reject: logical, True if the null is rejected at the chosen
            significance level.
        pval: probability, under the null hypothesis, of obtaining a test
            statistic at least as extreme as that which was observed.
        lf_stat: the sample KS statistic.
        lf_crit: the ksprb critical value.
        lf_null: nboot-length vector of the empirical null KS statistics.
    """

    # Input parsing:
    if not nboot: nboot = 1000
    if not alpha: alpha = 0.05

    # Get sample KS statistic
    lf_stat = kstest(y, 'norm')[0]

    # Get sample mean and variance
    ymean = np.mean(y)
    ystd = np.std(y)

    # Get FFT(y)
    Fy = np.fft.rfft(y)
    # Phase-randomize and get bootstrap KS statistic
    lf_null = []
    for jboot in range(0, nboot):
        yb = np.random.normal(loc=ymean,scale=ystd,size=len(y))
        lf_null.append(kstest(yb, 'norm')[0])

    # Estimate the p-value and critical value from the empirical CDF
    reject, pval, lf_crit = empirical_test(lf_stat,lf_null,alpha)

    # Return
    return reject, pval, lf_stat, lf_crit, lf_null

def ksprb(y, nboot=None, alpha=None):
    """Performs phase-randomized bootstrap KS test of normality.

    A variant of the Kolmogorov-Smirnov test of normality, permitting for the
    use of autocorrelated data. From the sample DFT, bootstrap samples are
    generated through uniform phase randomization. The resulting surrogates have
    identical sample autocorrelation functions, but are asymptotically normal.
    The KS statistic is computed for each to build up a distribution for the
    null, and the sample KS statistic is compared with this empirical estimate.

    ** Plan to add support for N-D arrays **

    Example:
import numpy as np
import normalitytests as nt
yn = np.random.normal(0.0,1.0,100) # Should not reject most of the time
H, pval, ks_stat, ks_crit, ks_null = nt.ksprb(yn,nboot=1000,alpha=0.05)
# OR:
yu = np.random.uniform(0.0,1.0,100) # Should reject most of the time
H, pval, ks_stat, ks_crit, ks_null = nt.ksprb(yu,nboot=1000,alpha=0.05)

    Args:
        y: a real vector, assumed to be uniformly sampled.
        nboot: Integer number of bootstrap samples to use (default is 1000).
        alpha: significance level (0 < alpha < 1, and default is 0.05).

    Returns:
        reject: logical, True if the null is rejected at the chosen
            significance level.
        pval: probability, under the null hypothesis, of obtaining a test
            statistic at least as extreme as that which was observed.
        ksprb_stat: the sample KS statistic.
        ksprb_crit: the ksprb critical value.
        ksprb_null: nboot-length vector of the empirical null KS statistics.
    """

    # Input parsing:
    if not nboot: nboot = 1000
    if not alpha: alpha = 0.05

    # Get sample KS statistic
    ksprb_stat = kstest(y, 'norm')[0]

    # Get FFT(y)
    Fy = np.fft.rfft(y)
    # Phase-randomize and get bootstrap KS statistic
    ksprb_null = []
    for jboot in range(0, nboot):
        Fyb = Fy * np.exp(np.random.uniform(low=0.,high=1.,size=Fy.shape) * 2*np.pi*1j)
        yb = np.fft.irfft(Fyb)
        ksprb_null.append(kstest(yb, 'norm')[0])

    # Estimate the p-value and critical value from the empirical CDF
    reject, pval, ksprb_crit = empirical_test(ksprb_stat,ksprb_null,alpha)

    # Return
    return reject, pval, ksprb_stat, ksprb_crit, ksprb_null

def monotone_fn_inverter(fn, x, vectorized=True, **keywords):
    """
    Given a monotone function x (no checking is done to verify monotonicity)
    and a set of x values, return an linearly interpolated approximation
    to its inverse from its values on x.
    """
    x = np.asarray(x)
    if vectorized:
        y = fn(x, **keywords)
    else:
        y = []
        for _x in x:
            y.append(fn(_x, **keywords))
        y = np.array(y)
    a = np.argsort(y)
    return interp1d(y[a], x[a])

def empirical_test(test_stat,null_stats,alpha):
    """
    Given a sample test statistic and those from a bootstrap null distribution,
    returns the result of the test (one-sided), the p-value, and the critical
    value. Note: currently uses linear interpolation to estimate the critical
    value, which may be inaccurate for small sample sizes.
    """
    ecdf = ECDF(null_stats)
    pval = 1. - ecdf(test_stat)
    if pval < alpha:
        reject = True
    else:
        reject = False
    eppf = monotone_fn_inverter(ecdf, ecdf.x)
    critval = np.asscalar(eppf(1. - alpha))
    return reject, pval, critval
