## Synopsis

A collection of statistical test of normality, with examples that compare their relative benefits and limitations. Includes the following tests:
*  `ks` -- Kolmogorov-Smirnov (standard: unit normal)
*  `lf` -- Lilliefors test (for normal, but *not* unit normal)
*  `ksprb` -- Phase-randomized Kolmogorov-Smirnov (for autocorrelated data)
*  `lfsb` -- Planned: Lilliefors test with stationary bootstrap (for autocorrelated data)

The `ksprb` test accounts for autocorrelation of the data, as well as the reduced number of degrees of freedom relative to the sample size in filtered data. The test is similar to the Lilliefors variation on the KS test, in that the distribution of the maximum deviation between two CDFs (the KS statistic) is computed via Monte-Carlo methods.  The Lilliefors test computes the null distribution by drawing i.i.d. samples from a normal distribution with parameters set to the sample mean and variance. The `ksprb` test computes the null as the distribution of the KS statistic for pairs of phase-randomized versions of the original sample. This ensures that the null has the same sample autocorrelation function and the same number of degrees of freedom as the original data.

## Code Examples
```
import numpy as np
import normalitytests as nt
yn = np.random.normal(0.0,1.0,100) # Should not reject most of the time
H, pval, ks_stat, ks_crit, ks_null = nt.ksprb(yn,nboot=100,alpha=0.05)
# OR:
yu = np.random.uniform(0.0,1.0,100) # Should reject most of the time
H, pval, ks_stat, ks_crit, ks_null = nt.ksprb(yu,nboot=100,alpha=0.05)
```

The standard `ks` test is performed against the null of a unit normal distribution, so generally you will want to normalize first (though note that this results in the loss of a degree of freedom, biasing the standard KS test!):
```
import numpy as np
import normalitytests as nt
ynn = np.random.normal(-2.0,5.0,100)
# Should reject most of the time without standardization
H, pval, ks_stat, ks_crit, ks_null = nt.ks(ynn,nboot=100,alpha=0.05)
# But should not reject most of the time if standardized first
from sklearn.preprocessing import scale
H, pval, ks_stat = nt.ks(scale(ynn),alpha=0.05)
```


## Motivation

## Installation

## Tests

```python -m unittest -v test_normalitytests```

## Contributors

## License

GPL3
